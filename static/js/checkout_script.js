$(window).on("load",function(){
  function iOSversion() {
    // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
      var v = (navigator.appVersion).match(/Android/);
      return v;
  }

  ver = iOSversion();
  if(ver === null){
    $(".second-step__packages").mCustomScrollbar({
      theme: "dark",
      axis: "y",
      autoDraggerLength: true
    });
    $(".second-step__header-checkout-body").mCustomScrollbar({
      theme: "dark",
      axis: "y",
      autoDraggerLength: true
    });
    // $(".second-step__booking-treatments_ready").mCustomScrollbar({
    //   theme: "dark",
    //   axis: "y",
    //   autoDraggerLength: true
    // });
  }


});

$(window).resize(function(){
  if($(window).width() <= 375){
    $('.main-content__packeges-services').mCustomScrollbar("destroy");
  }
});

$(document).ready(function(){
  $('#address').select2({
    placeholder: "Select your address"
  });

  $('.selection').click(function(){
    $('.select2-search__field').attr('readonly', true);
  });

  $("body").on("click", ".select2-search__field", function(){
    $('.select2-search__field').attr('readonly', false);
  });

  function paymentInputCheck(selector){
    if (
      $('.second-step__payment-form input[type="checkbox"]').prop('checked')
     ) {
       $('.second-step__payment-details-next').css('opacity', '1');
       $('.second-step__payment-details-next').attr('disabled', false);

     }else{
       $('.second-step__payment-details-next').css('opacity', '0.3');
       $('.second-step__payment-details-next').attr('disabled', true);
     }
  }

  $('.second-step__payment-form input[type="text"]').change(function () {
    paymentInputCheck();

  });
  $('.second-step__payment-form input[type="tel"]').change(function () {
    paymentInputCheck();
  });

  $('.second-step__payment-form input[type="checkbox"]').change(function () {
    paymentInputCheck(this);
  });

  
});
//end ready

function ssKeypress(numb){
		var charNow = $('.second-step__ss-all').val();
		charNow = charNow[numb];

		setTimeout(function(){
			$('.second-step__ss-wrap').eq(numb).addClass('done').children('input').val(charNow);
			$('.second-step__ss-wrap').eq(numb+1).addClass('focus');
		}, 100);

		setTimeout(function(){
			$('.second-step__ss-wrap').eq(numb).addClass('fine')
		}, 200);
	};

	// verify code
	$('.second-step__ss-all').on('keypress keyup keydown', function(e) {

		var thisEl = $(this);

		var len = $(this).val().length;

		if ( (e.which === 8) || (e.which === 46) ){
			$('.second-step__ss-wrap').removeClass('done').removeClass('focus')
				.removeClass('fine').first().addClass('focus');
			$('.second-step__ss_input').val('');
			$('.second-step__ss-all').val('');

			return false;
		};

		if (len === 1) {

			ssKeypress(0);

		} else if (len === 2){

			ssKeypress(1);


		} else if (len === 3){

			ssKeypress(2);

		} else if (len >= 4){

			thisEl.attr('readonly', 'readonly');

			ssKeypress(3);

		};
	});

	$('.second-step__ss_input').click(function(e) {
		e.preventDefault();
		$('.second-step__ss_input').val('');
		$('.second-step__ss-wrap')
			.removeClass('done')
			.removeClass('fine');
		setTimeout(function(){
			$('.second-step__ss-all').val('').removeAttr('readonly').focus();
			$('.second-step__ss-wrap')
				.first().addClass('focus');
		}, 100);
	});

	$('.second-step__ss-all').focusout(function(e) {
		setTimeout(function(){
			jQuery('.second-step__ss-wrap').removeClass('focus');
		}, 100);
	});
