(function($) {
  $(function() {
  	//	Files Upload
  	// $('.file-uploader').each(function(){
  	// 	var _self = $(this), _file,
  	// 			_input = _self.find('input[type="file"]'),
  	// 			_inputUrl = _self.find('input[type="hidden"]'),
  	// 			_form = _self.find('.form-border'),
  	// 			_bar = _self.find('.uploader-progress-bar'),
  	// 			_sendBtn = _self.find('.btn-send'),
  	// 			_chooseBtn = _self.find('.btn-choose'),
  	// 			_progressBarLine = _self.find('.progress-bar-line'),
  	// 			_fileStatus = _self.find('.file-status'),
  	// 			_fileNameTag = _self.find('.file-name'),
  	// 			_fileDelete = _self.find('.file-delete a'),
  	// 			formdata = new FormData(),
  	// 			ajax = new XMLHttpRequest();

  	// 	_chooseBtn.on('click' , function(){
  	// 		_input.trigger('click');
  	// 		return false;
  	// 	});
	//   	_input.on('change', function(e){
	//   		if(e.currentTarget.files && e.currentTarget.files.length > 0)
	//   		{
	//   			_file = e.currentTarget.files[0];
	//   			if(_file.size > 2*1024*1024)
	//   			{
	// 					_fileNameTag.text(_file.name + ' is too large');
	// 					_file = false;
	// 					_sendBtn.parent().addClass('hidden');
	// 					_chooseBtn.parent().removeClass('hidden');
	//   			}
	//   			else
	//   			{
	// 					_fileNameTag.text(_file.name);
	// 					_sendBtn.parent().removeClass('hidden');
	// 					_chooseBtn.parent().addClass('hidden');
	//   			}
	//   		}
	//   	});
	//   	_sendBtn.on('click', function(){
	//   		_bar.removeClass('hidden');
	//   		_form.addClass('hidden');

	// 		  formdata.append("file1", _file);
	// 		  ajax.open("POST", "file_upload_parser.php");
	// 		  ajax.send(formdata);
	//   	});
	// 	  ajax.upload.addEventListener("progress", function(event) {
	// 		  var percent = (event.loaded / event.total) * 100;
	// 		  _progressBarLine.css('width', percent + '%');
	// 		  _fileStatus.text("Uploading…");
	// 		}, false);
	// 	  ajax.addEventListener("load", function(event) {
	// 	  	console.log(event);
	// 	  	if(event.target.response)
	// 	  	{
	// 	  		var response = $.parseJSON( event.target.response );
	// 	  		if(response.error)
	// 	  		{
	// 	  			_fileNameTag.text(response.error);
	// 	  			_fileStatus.text("Upload Failed…");
	// 	  		}
	// 	  		else
	// 	  		{
	// 			  	_inputUrl.val(response.file);
	// 				  _fileStatus.text('Upload is complete');
	// 				  _progressBarLine.css('width', '100%');
	// 				  $('.step-documents').trigger('checkForms');
	// 	  		}
	// 	  	}
	// 	  	else
	// 	  	{
	// 	  		_fileStatus.text("Upload Failed…");
	// 	  	}
	// 		}, false);
	// 	  ajax.addEventListener("error", function(event) {
	// 		  _fileStatus.text("Upload Failed…");
	// 		}, false);
	// 	  ajax.addEventListener("abort", function(event) {
	// 		  _fileStatus.text("Upload Aborted… Please wait…");
	// 		}, false);
	// 	  _fileDelete.on('click', function(){
	// 	  	ajax.abort();
	// 	  	_fileNameTag.text('');
	// 	  	_inputUrl.val('');
	// 	  	_fileStatus.text("Upload Aborted… Please wait…");
	// 	  	setTimeout(function(){
	// 	  		formdata = new FormData();
	// 	  		_progressBarLine.css('width', 0);
	// 	  		_file = false;
	// 				_sendBtn.parent().addClass('hidden');
	// 				_chooseBtn.parent().removeClass('hidden');
	// 	  		_input.val('');
	// 	  		_bar.addClass('hidden');
	// 	  		_form.removeClass('hidden');
	// 	  	},3000);
	// 	  	return false;
	// 	  });
  	// });
  	
  	$('.step-documents').on('checkForms', function(){
  		var error = false;
  		$(this).find('input[required]').each(function(){
  			if(!$(this).val())
  			{
  				error = true;
  			}
  		});
			if(!$('#agreement').is(':checked'))
			{
				error = true;
			}
			if(error === false)
			{
				$('.step-documents input[type=submit]').removeAttr('disabled');
			}
			else
			{
				$('.step-documents input[type=submit]').attr('disabled','disabled');
			}
  	}).trigger('checkForms');

  	//	Disable carousel slide
  	var currentSlideIndex;
  	$('#carousel-workflowver').carousel({
		  interval: false
		}).on('slid.bs.carousel', function (e) {
		  currentSlideIndex = $('#carousel-workflowver .active').index('#carousel-workflowver .item');
		  console.log(currentSlideIndex);
		  $('.steps ul li').removeClass('current half');
		  if(currentSlideIndex == 1)
		  {
		  	$('.steps ul li').eq(currentSlideIndex).addClass('half').prevAll().addClass('current');
		  }
		  else if(currentSlideIndex > 1)
		  {
		  	$('.steps ul li').eq(currentSlideIndex - 1).addClass('current').prevAll().addClass('current');
		  }
		  else
		  {
		  	$('.steps ul li').eq(currentSlideIndex).addClass('current');
		  }

		  $('html, body').animate({
		    scrollTop: $('#join').offset().top
		  }, 1500);
		});

	// $('.steps ul li').on('click', function(){
	// 	if($(this).hasClass('current'))
	// 	{
	// 		$('#carousel-workflowver').carousel($(this).data('step'));
	// 	}
	// 	return false;
	// });
		//	Go to step 2
		// $('.step-postcode input[type=submit], .checkcode-resend a').on('click', function(){
		// 	$('#carousel-workflowver').carousel(1);
		// 	return false;
		// });
		//	Go to step 3
		// $('.step-phone input[type=submit]').on('click', function(){
		// 	$('#carousel-workflowver').carousel(2);
		// 	return false;
		// });
		//	Go to step 4
		// $('.step-phone-verify input[type=submit]').on('click', function(){
		// 	$('#carousel-workflowver').carousel(3);
		// 	return false;
		// });
		//	Go to step 5
		// $('.step-contact input[type=submit]').on('click', function(){
		// 	var error = false;
		// 	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		// 	$('.step-contact .form-group').each(function(){
		// 		if($(this).find('.form-control').val() == '' || ($(this).find('.form-control').attr('type') == 'email' && !re.test(String($(this).find('.form-control').val()).toLowerCase())))
		// 		{
		// 			$(this).addClass('has-error');
		// 			error = true;
		// 		}
		// 		else
		// 		{
		// 			$(this).removeClass('has-error');
		// 		}
		// 	});
		// 	if(error == false)
		// 	{
		// 		$('#carousel-workflowver').carousel(4);
		// 	}
		// 	return false;
		// });
		//	Go to step 5
		// $('.step-documents input[type=submit]').on('click', function(){
		// 	if($('#agreement').is(':checked'))
		// 	{
		// 		$('#carousel-workflowver').carousel(5);
		// 	}
		// 	else
		// 	{
		// 		$('#agreement').parent().addClass('has-error');
		// 	}
		// 	return false;
		// });
		// $('#agreement').on('change', function(){
		// 	$('.step-documents').trigger('checkForms');
		// });
		
		//	Phone mask
		//$(".phone-mask").mask("(+44) 999999999999999",{placeholder:""});
		$(".input-number").mask("9",{
			placeholder:"",
			completed: function(){
				var _self = $(this);
				setTimeout(function(){
					_self.addClass('verify');
					_self.next().focus();
					//console.log(_self, _self.next());
				},0);	
				/*if($('.checkcode .verify').length == 4)
				{
					$('#carousel-workflowver').carousel(3);
				}*/
			}
		});

  	$('[data-animation]').each(function(){
  		if($(window).width() >= 768)
  		{
		    if($(this).data('animated-delay'))
		    {
		    	$(this).css('-webkit-animation-delay',$(this).data('animated-delay'))
		    			.css('-moz-animation-delay',$(this).data('animated-delay'))
		    			.css('-o-animation-delay',$(this).data('animated-delay'))
		    			.css('animation-delay',$(this).data('animated-delay'));
		    }
		    $(this).css('opacity',0);
				$(this).onScreen({
					container: window,
					direction: 'vertical',
					tolerance: 200,
					doIn: function() {
		    		$(this).css('opacity', 1);
						$(this).addClass('animated ' + $(this).data('animation'));
					}
				});
  		}
  	});

		$(document).on('click', 'a[href^="#"]', function (event) {
		  event.preventDefault();
		  $('html, body').animate({
		    scrollTop: $($.attr(this, 'href')).offset().top
		  }, 1500);
		});
  });
})(jQuery);