$(document).ready(function(){
    var is_promo_closed = false;
    $('.notification__close-btn').click(function(){
        $('.notification__close-btn').closest('.notification').fadeOut('fast');
        is_promo_closed = true;
    });

    $('.first-step__join_button').click(function(){
        window.location.href = '/register.html';
    });

    $('.first-step__form').submit(function(e){
        e.preventDefault();
        var postcode = $('.first-step__form_input-text').val();
        if(postcode.length > 0){
            var self = this;
            $('.error-code').hide();

            axios.get(echic_config.$baseUrl + '/api/utilities/CheckPostalCode/' + postcode)
            .then(function (response) {
                if(response.data.isAllowed && response.data.isExist && response.status === 200){
                    var rediration_url = '/order.html?postcode=' + postcode;
                    if(is_promo_closed){
                        rediration_url += '&promo=false'; 
                    }
                    window.location.href = rediration_url;
                } else {
                    $('.error-code').show();
                    $('.error-code').text("Sorry, it seems we don't work in your area yet");
                }
            })
            .catch(function (error) {
                $('.error-code').show();
                $('.error-code').text('Sorry, connection error');
            })
            .then(function(){
                self.isBusy = false;
            });
        }
    });

    $('.notification__close-btn').click(function () {
        $('.notification__close-btn').closest('.notification').fadeOut('fast');
    });
});