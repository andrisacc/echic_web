var echic_config = {};

var deployed = 'local';//'test','prod'
if(deployed === 'prod'){
    echic_config.$baseUrl = './';
    echic_config.$gtagid = 'UA-118261528-1';
} else if(deployed === 'test') {
    if(typeof Vue != "undefined"){
        Vue.config.devtools = true;
    }
    echic_config.$baseUrl = './';
    echic_config.$gtagid = 'UA-118697587-1';
} else if(deployed === 'local') {
    if(typeof Vue != "undefined"){
        Vue.config.devtools = true;
    }
    echic_config.$baseUrl = 'http://localhost:60907/';
    echic_config.$gtagid = 'UA-118697587-1';
}