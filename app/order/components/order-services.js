Vue.component('order-services', {
    props: [
        'servicesGroups',
        'services',
        'currentStep',
        'purchases'
    ],
    methods: {
        isOpen: function(stepName){
            return stepName === this.currentStep.step;
        },
        isDone: function(stepName){
            return this.currentStep.steps.indexOf(stepName) < this.currentStep.steps.indexOf(this.currentStep.step);
        },
        onGroupChanged: function(uiId){
            this.servicesGroups.forEach(sg => {
                sg.isSelected = sg.uiId === uiId;
            });
        },
        serviceAdded: function(event){
            this.$emit('serviceAdded', event);
        },
        backStep: function(){
            this.$emit('backStep', event);
        },
        nextStep: function(){
            this.$emit('nextStep', event);
        }
    },
    template:
    `
    <div class="second-step__checkout-step second-step__main-step_1"
        v-bind:class="[isOpen('services')?'open':'', isDone('services')?'done':'']">

        <div class="second-step__tabs">
            <div v-for="sg in servicesGroups" class="second-step__tabs-item" 
                v-bind:class="{active: sg.isSelected}"
                v-on:click="onGroupChanged(sg.uiId)">
                <img v-bind:src="sg.pictureUrl" alt="">
                <span>{{sg.title}}</span>
            </div>
        </div>
        
        <order-servicesgroup 
            v-for="(sg,index) in servicesGroups" 
            v-bind:servicesGroup="sg" 
            v-bind:services="services"
            v-on:serviceAdded="serviceAdded($event)"
            v-bind:purchases="purchases"
            v-on:nextStep="nextStep"
            v-on:backStep="backStep"></order-servicesgroup>
    </div>
    `
});