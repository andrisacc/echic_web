Vue.component('order-wizard', {
    data: function () {
        return {
            postcodeResult: {},
            servicesGroups: [{ isSelected: false }],
            services: [],
            isPurchasesEditable: true,
            purchases: [],
            discount: 50,
            currentStep: {
                steps: ["postcode", "services", "time", "phonenumber", "code", "contacts", "payment", "complite"],
                step: "postcode"
            },
            totalDuration: 0,
            selectedDate: {
                time: {
                    timeH: 0,
                    timeM: 0
                },
                date: {
                    year: 2001,
                    month: 1,
                    day: 1
                }
            },
            shouldShowDate: false,
            contactsInfo: {},
            orderId: "",
            orderNumber: "",
            postcode: ""
        }
    },
    created: function () {
        this.currentStep.step = "services";

        self = this;
        axios.get(echic_config.$baseUrl + 'api/utilities/GetServicesGroups')
            .then(function (sgresponse) {
                if (sgresponse.status === 200) {
                    self.servicesGroups = sgresponse.data;
                    axios.get(echic_config.$baseUrl + 'api/utilities/GetServices')
                        .then(function (sresponse) {
                            if (sresponse.status === 200) {
                                self.services = sresponse.data;

                                setTimeout(function () {
                                    if ((navigator.appVersion).match(/Android/) === null) {
                                        $(".second-step__packages").mCustomScrollbar({
                                            theme: "dark",
                                            axis: "y",
                                            autoDraggerLength: true
                                        });
                                        $(".second-step__header-checkout-body").mCustomScrollbar({
                                            theme: "dark",
                                            axis: "y",
                                            autoDraggerLength: true
                                        });
                                    }
                                }, 500)
                            }
                        });
                }
            });

            var url_string = window.location.href
            var url = new URL(url_string);
            this.postcode = url.searchParams.get("postcode");

            var promo = url.searchParams.get("promo");
            if(promo!=null){
                $('.notification__close-btn').closest('.notification').fadeOut('fast');
            }
    
            var self = this;
            if(this.postcode != null && this.postcode != undefined && this.postcode.length > 0){
                axios.get(echic_config.$baseUrl + 'api/utilities/CheckPostalCode/' + this.postcode)
                .then(function (response) {
                    if(response.data.isAllowed && response.data.isExist && response.status === 200){
                        self.postcodeResult = response.data;
                        self.currentStep.step = "services";
                    } else {
                        self.currentStep.step = "postcode"
                    }
                })
                .catch(function (error) {
                    self.currentStep.step = "postcode"
                });
            } else {
                var rediration_url = '/index.html';
                window.location.href = rediration_url;
            }
    },
    methods: {
        isOpen: function (stepName) {
            return stepName === this.currentStep.step;
        },
        isDone: function (stepName) {
            return this.currentStep.steps.indexOf(stepName) < this.currentStep.steps.indexOf(this.currentStep.step);
        },
        postcodeNextStep: function (event) {
            this.postcodeResult = event;
            this.currentStep.step = "services";

            if ($(window).width() <= 480) {
                $('.second-step__packages').mCustomScrollbar("destroy");
                $('.second-step__packages').height('auto');
            }
        },
        serviceAdded: function (event) {
            var existedItem = this.purchases.filter(p => p.id === event.id);
            if (existedItem.length > 0) {
                if (existedItem[0].quantity + event.quantity < 3) {
                    existedItem[0].quantity += event.quantity;
                } else {
                    existedItem[0].quantity = 3;
                }
            } else {
                this.purchases.push(Object.assign({}, event));
            };

            var duration = 0;
            this.purchases.forEach(purchase => {
                duration += purchase.duration * purchase.quantity;
            });
            this.totalDuration = duration;
        },
        purchaseRemoved: function (item) {
            var indexToRemove = this.purchases.indexOf(item);
            this.purchases.splice(indexToRemove, 1);
        },
        servicesBackStep: function () {
            this.currentStep.step = "postcode";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        servicesNextStep: function () {
            this.currentStep.step = "time";
            this.isPurchasesEditable = false;
            this.shouldShowDate = true;
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        timeBackStep: function () {
            this.currentStep.step = "services";
            this.isPurchasesEditable = true;
            this.shouldShowDate = false;
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        timeNextStep: function () {
            this.currentStep.step = "phonenumber";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        dateSelected: function (date) {
            this.selectedDate = date;
        },
        contactsBackStep: function () {
            this.currentStep.step = "time";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        contactsNextStep: function (event) {
            this.contactsInfo = event;
            var self = this;

            axios.put(echic_config.$baseUrl + 'api/Order/Create/', {
                PostalCode: this.postcodeResult.postcode,
                PhoneCode: this.postcodeResult.phoneCode,
                PhoneNumber: this.contactsInfo.phoneInfo.phoneNumber,
                FirstName: this.contactsInfo.contacts.firstName,
                LastName: this.contactsInfo.contacts.secondName,
                Email: this.contactsInfo.contacts.email,
                Address: this.contactsInfo.contacts.address,
                City: this.contactsInfo.contacts.city,
                LocationId: this.postcodeResult.locationId,
                Purchases: this.purchases,
                RequestedDateTimeLocal: this.selectedDate.date.year + '.'
                    + this.selectedDate.date.month + '.'
                    + this.selectedDate.date.day + ' '
                    + this.selectedDate.time.timeH + ':' + this.selectedDate.time.timeM
            }).then(function (response) {
                if (response.status === 200 && response.data) {
                    self.orderId = response.data.orderId;
                    self.orderNumber = response.data.orderNumber;
                    self.currentStep.step = "payment";
                    $('body').scrollTop($(".second-step__content").offset().top);
                } else {
                    self.isWrongCode = true;
                }
            })
                .catch(function (error) {
                    console.log(error);
                });
        },
        paymentBackStep: function () {
            this.currentStep.step = "contacts";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        paymentNextStep: function () {
            this.currentStep.step = "complite";

            if ($(window).width() <= 480) {
                $('.second-step__packages').mCustomScrollbar("destroy");
                $('.second-step__packages').height('auto');
            }

            $('body').scrollTop(0);
        },
        compliteNextStep: function () {
            window.location.reload(false);
        },
        jointeam: function () {
            window.location.href = '/register.html';
        }
    },
    mounted: function () {
        $(window).on("load", function () {




        });

        $(window).resize(function () {
            if ($(window).width() <= 375) {
                if ($(window).width() <= 480) {
                    $('.second-step__packages').mCustomScrollbar("destroy");
                    $('.second-step__packages').height('auto');
                } else {
                    $(".second-step__packages").mCustomScrollbar({
                        theme: "dark",
                        axis: "y",
                        autoDraggerLength: true
                    });
                    $('.second-step__packages').height('455px');
                }
            }
        });

        $(document).ready(function () {
            $('#address').select2({
                placeholder: "Select your address"
            });

            $('.selection').click(function () {
                $('.select2-search__field').attr('readonly', true);
            });

            $("body").on("click", ".select2-search__field", function () {
                $('.select2-search__field').attr('readonly', false);
            });

            function paymentInputCheck(selector) {
                if (
                    $('.second-step__payment-form input[type="checkbox"]').prop('checked')
                ) {
                    $('.second-step__payment-details-next').css('opacity', '1');
                    $('.second-step__payment-details-next').attr('disabled', false);

                } else {
                    $('.second-step__payment-details-next').css('opacity', '0.3');
                    $('.second-step__payment-details-next').attr('disabled', true);
                }
            }

            $('.second-step__payment-form input[type="text"]').change(function () {
                paymentInputCheck();

            });
            $('.second-step__payment-form input[type="tel"]').change(function () {
                paymentInputCheck();
            });

            $('.second-step__payment-form input[type="checkbox"]').change(function () {
                paymentInputCheck(this);
            });

            $('.notification__close-btn').click(function () {
                $('.notification__close-btn').closest('.notification').fadeOut('fast');
            });
        });
        //end ready

        function ssKeypress(numb) {
            var charNow = $('.second-step__ss-all').val();
            charNow = charNow[numb];

            setTimeout(function () {
                $('.second-step__ss-wrap').eq(numb).addClass('done').children('input').val(charNow);
                $('.second-step__ss-wrap').eq(numb + 1).addClass('focus');
            }, 100);

            setTimeout(function () {
                $('.second-step__ss-wrap').eq(numb).addClass('fine')
            }, 200);
        };

        // verify code
        $('.second-step__ss-all').on('keypress keyup keydown', function (e) {

            var thisEl = $(this);

            var len = $(this).val().length;

            if ((e.which === 8) || (e.which === 46)) {
                $('.second-step__ss-wrap').removeClass('done').removeClass('focus')
                    .removeClass('fine').first().addClass('focus');
                $('.second-step__ss_input').val('');
                $('.second-step__ss-all').val('');

                return false;
            };

            if (len === 1) {

                ssKeypress(0);

            } else if (len === 2) {

                ssKeypress(1);


            } else if (len === 3) {

                ssKeypress(2);

            } else if (len >= 4) {

                thisEl.attr('readonly', 'readonly');

                ssKeypress(3);

            };
        });

        $('.second-step__ss_input').click(function (e) {
            e.preventDefault();
            $('.second-step__ss_input').val('');
            $('.second-step__ss-wrap')
                .removeClass('done')
                .removeClass('fine');
            setTimeout(function () {
                $('.second-step__ss-all').val('').removeAttr('readonly').focus();
                $('.second-step__ss-wrap')
                    .first().addClass('focus');
            }, 100);
        });

        $('.second-step__ss-all').focusout(function (e) {
            setTimeout(function () {
                jQuery('.second-step__ss-wrap').removeClass('focus');
            }, 100);
        });

    },
    template:
        `<div class="steps-wrapper">

            <div class="steps-wrap first-step" 
                v-bind:class="[isOpen('postcode')?'open':'', isDone('postcode')?'done':'']">
                <header class="first-step__header">
                    <div class="container">
                        <div class="first-step__header-wrap">
                            <div class="first-step__header-logo">
                                <a href="/">
                                    <img src="/static/images/echic_logo.svg" alt="logo">
                                </a>
                            </div>
                            <button class="first-step__join_button" v-on:click="jointeam">Join eChíc team</button>
                        </div>

                        <div class="first-step__content-wrapper">
                            <span class="first-step__subtitle">Your chíc on demand</span>
                            <h1 class="first-step__title">Beauty treatments at your home and office</h1>
                            <order-postcode v-on:nextStep="postcodeNextStep($event)" v-bind:postcode='postcode'></order-postcode>
                        </div>
                    </div>
                </header>
            </div>

            <div class="steps-wrap second-step" 
                v-bind:class="[(isOpen('services')||isOpen('time')||isOpen('phonenumber')||isOpen('code')||isOpen('contacts')||isOpen('payment'))?'open':'',
                (isDone('services')&&isDone('time')&&isDone('phonenumber')&&isDone('code')&&isDone('contacts')&&isDone('payment'))?'done':'']">
                <header class="second-step__header">
                    <div class="container">
                        <div class="first-step__header-logo">
                            <a href="/">
                                <img src="/static/images/echic_logo.svg" alt="logo">
                            </a>
                        </div>
                        <order-checkout-short v-bind:purchases="purchases"></order-checkout-short>
                    </div>
                </header>
                <div class="second-step__content">
                    <div class="container">
                        <order-services 
                            v-bind:servicesGroups="servicesGroups"
                            v-bind:services="services"
                            v-bind:currentStep="currentStep"
                            v-bind:purchases="purchases"
                            v-on:serviceAdded="serviceAdded($event)"
                            v-on:nextStep="servicesNextStep"
                            v-on:backStep="servicesBackStep"></order-services>

                        <order-timeselection
                            v-bind:currentStep="currentStep"
                            v-on:nextStep="timeNextStep"
                            v-bind:totalDuration="totalDuration"
                            v-on:backStep="timeBackStep"
                            v-on:dateSelected="dateSelected"></order-timeselection>

                        <order-contacts
                            v-bind:currentStep="currentStep"
                            v-on:nextStep="contactsNextStep"
                            v-on:backStep="contactsBackStep"
                            v-bind:addresses="postcodeResult.addresses"
                            v-bind:phoneCode="postcodeResult.phoneCode"></order-contacts>

                        <order-payment
                            v-bind:currentStep="currentStep"
                            v-bind:orderId="orderId"
                            v-on:nextStep="paymentNextStep"
                            v-on:backStep="paymentBackStep"></order-payment>
                    </div>
                </div>
            </div>

            <div class="steps-wrap complite" v-bind:class="[isOpen('complite')?'open':'', isDone('complite')?'done':'']">
                <order-complite v-on:nextStep="compliteNextStep"></order-complite>
            </div>

            <order-checkout 
                v-bind:purchases="purchases" 
                v-bind:isEditable="isPurchasesEditable" 
                v-bind:discount="discount"
                v-bind:shouldShowDate="shouldShowDate"
                v-bind:selectedDate="selectedDate"
                v-on:purchaseRemoved="purchaseRemoved"></order-checkout>
        </div>`
});