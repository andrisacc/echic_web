Vue.component('order-date', {
    data: function () {
        return {
            allowedDays: [],
            isCalendarLoaded: false
        }
    },
    props: ['totalDuration'],
    methods:{
        calendarInit: function(){
            self = this;
            if(this.isCalendarLoaded){
                $('#calendar').datepicker("refresh");
            } else {
                $('#calendar').datepicker({
                    inline: true,
                    firstDay: 1,
                    showOtherMonths: true,
                    dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    beforeShowDay: function(date){

                        var day = self.allowedDays.filter(d=>d.day == date.getDate() && d.month == (date.getMonth()+1) && d.year == date.getFullYear())[0];

                        if(day != undefined){
                            return [ day.isAllowed ] ;
                        } else {
                            return [ false ];
                        }
                        
                        
                    },
                    onSelect: function(date){
                        var date = $('#calendar').datepicker("getDate");

                        var day = self.allowedDays.filter(d=>d.day == date.getDate() && d.month == (date.getMonth()+1) && d.year == date.getFullYear())[0];

                        self.$emit('dateSelected', day);
                    }
                });
                this.isCalendarLoaded = true;

                var defaultSelectedDate;
                self.allowedDays.some(function(element) {
                    if(element.isAllowed){
                        defaultSelectedDate = element;
                        return defaultSelectedDate;
                    }
                });

                var d = new Date(defaultSelectedDate.year, defaultSelectedDate.month - 1, defaultSelectedDate.day); 

                $( '#calendar' ).datepicker( "setDate", d );
            }
        }
    },
    watch: {
        totalDuration: function(newItem, oldItem){
            var self = this;

            var today = new Date();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();

            axios.post(echic_config.$baseUrl + 'api/utilities/GetAllowedTime',{
                    Year: year,
                    Month: month,
                    TotalDuration: self.totalDuration
                })
                .then(function (dresponse) {
                    if(dresponse.status === 200){
                        self.allowedDays = dresponse.data;
                        self.calendarInit();

                        var defaultSelectedDate;
                        self.allowedDays.some(function(element) {
                            if(element.isAllowed){
                                defaultSelectedDate = element;
                                return defaultSelectedDate;
                            }
                        });

                        self.$emit('dateSelected', defaultSelectedDate);
                    } 
                });
        }
    },
    created: function(){
        
    },
    template:
    `
        <div id="calendar" class="second-step__calendar"></div>
    `
 });