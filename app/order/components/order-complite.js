Vue.component('order-complite', {
    data: function () {
        return {
        }
    },
    methods:{
        nextStep: function(event){
            event.preventDefault();
            this.$emit('nextStep', event);
        }
    },
    template:
    `
    <main class="complite__content">
        <div class="complite__content-body">
            <div class="complite__content_logo">
                <img src="/static/images/echic_logo.svg" alt="logo">
            </div>
            <div class="complite__content-congratulations">
                <img src="/static/images/checkout/complete_icon.jpg" alt="complete_icon">
                <h2 class="complite__content_title">Congratulations!
                <br> Your treatment has been booked</h2>
                <div class="complite__content_text">
                    <p>You should receive the confirmation email shortly. Our system will match your booking with an eChíc beautician
                        and provide you with their contact details.</p>
                </div>
                <a href="./index.html" class="complite__main-page_btn">Back to main page</a>
            </div>
        </div>
    </main>`
 });