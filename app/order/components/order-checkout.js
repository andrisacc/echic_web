Vue.component('order-checkout', {
    data: function(){
        return {
            selectedDateTime: null
        }
    },
    props: [
        'purchases',
        'isEditable',
        'discount',
        'shouldShowDate',
        'selectedDate'
    ],
    methods: {
        purchaseRemoved: function(item){
            this.$emit('purchaseRemoved', item);
        }
    },
    computed: {
        treatmentsCount: function(){
            return this.purchases.length;
        },
        totalDuration: function(){
            var duration = 0;
            if(this.purchases.length > 0){
                this.purchases.forEach(item => {
                    duration += item.quantity*item.duration;
                });
            }

            var hours = Math.trunc(duration/60);
            var mins = duration%60;

            return (hours>0?hours + "h ":"") + mins + "min";
        },
        totalSum: function(){
            var sum = 0;
            if(this.purchases.length > 0){
                this.purchases.forEach(item => {
                    sum += item.quantity*item.cost/100;
                });
            }

            if(this.discount>0){
                sum = sum*this.discount/100;
            }

            return sum.toFixed(2);
        },
        orderDateTime: function(){
            return new Date(this.selectedDate.date.year, this.selectedDate.date.month - 1, this.selectedDate.date.day, this.selectedDate.time.timeH, this.selectedDate.time.timeM, 0);
        },
        orderDate: function(){
            return this.orderDateTime.getDate();
        },
        orderHours: function(){
            var h = this.orderDateTime.getHours();
            return h.toString().length>1?h:'0'+h;
        },
        orderMinutes: function(){
            var m = this.orderDateTime.getMinutes();
            return m.toString().length>1?m:'0'+m;
        },
        orderYear: function(){
            return this.orderDateTime.getFullYear();
        },
        orderDayOfWeek: function(){
            var d = this.orderDateTime;
            if(d.getDay()==0){ return "Sunday" };
            if(d.getDay()==1){ return "Monday" };
            if(d.getDay()==2){ return "Tuesday" };
            if(d.getDay()==3){ return "Wednesday" };
            if(d.getDay()==4){ return "Thursday" };
            if(d.getDay()==5){ return "Friday" };
            if(d.getDay()==6){ return "Saturday" };
        },
        orderMonth: function(){
            var d = this.orderDateTime;
            if(d.getMonth()==0){return "January"};
            if(d.getMonth()==1){return "February"};
            if(d.getMonth()==2){return "March"};
            if(d.getMonth()==3){return "April"};
            if(d.getMonth()==4){return "May"};
            if(d.getMonth()==5){return "June"};
            if(d.getMonth()==6){return "July"};
            if(d.getMonth()==7){return "August"};
            if(d.getMonth()==8){return "September"};
            if(d.getMonth()==9){return "October"};
            if(d.getMonth()==10){return "November"};
            if(d.getMonth()==11){return "December"};
        }
    },
    template:
    `
    <div class="second-step__header-checkout-body">
        <div class="second-step__booking_title">Booking summary
        </div>

        <div class="second-step__booking-datetime" v-show="shouldShowDate">
            <div class="second-step__booking-subtitle">Date & Time</div>
            <div class="second-step__booking-datetime_wrap">
                <div class="second-step__booking_day">{{orderDate}}</div>
                <div class="second-step__booking_date">
                    <span>{{orderDayOfWeek}}, {{orderHours}}:{{orderMinutes}}</span>
                    <span>{{orderMonth}} {{orderYear}}</span>
                </div>
            </div>
        </div>

        <div class="second-step__booking-treatments">
            <div v-bind:class="[isEditable ? 'second-step__booking-treatments_editable' : 'second-step__booking-treatments_ready']">
                <order-checkout-item 
                    v-for="purchase in purchases" 
                    v-bind:isEditable="isEditable" 
                    v-bind:purchase="purchase"
                    v-on:purchaseRemoved="purchaseRemoved"></order-checkout-item>
            </div>
        </div>

        <div class="second-step__booking-summary">
            <div class="second-step__booking-subtitle">summary</div>
            <div class="second-step__booking-summary_wrap">
                <div class="second-step__booking-duration-row">
                    <div class="second-step__booking-duration_title">Duration</div>
                    <div class="second-step__booking-duration_count">{{totalDuration}}</div>
                </div>
                <div class="second-step__discount-applied" v-show="discount>0">{{discount}}% Discount Applied</div>
                <div class="second-step__booking-total-row">
                    <div class="second-step__booking-total_title">Total</div>
                    <div class="second-step__booking-total_count">
                        <div class="second-step__booking-total_count-old-price hide">£ {{totalSum}}</div>
                        <div class="second-step__booking-total_count-price">£ {{totalSum}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
 });