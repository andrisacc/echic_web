Vue.component('order-payment', {
    data: function () {
        return {
            stripe: {},
            card: {},
            IsAggrementChecked: false,
            IsFinishButtonDisabled: true,
            isBusy: false,
            isCardInvalid: false
        }
    },
    props: [
        'currentStep',
        'orderId'
    ],
    methods:{
        isOpen: function(stepName){
            return stepName === this.currentStep.step;
        },
        isDone: function(stepName){
            return this.currentStep.steps.indexOf(stepName) < this.currentStep.steps.indexOf(this.currentStep.step);
        },
        backStep: function(event){
            event.preventDefault();
            this.$emit('backStep', event);
        },
        nextStep: function(event){
            event.preventDefault();
            this.$emit('nextStep', event);
        },
        onSubmit: function(){
            var self = this;

            $('.modal-waiting').css('visibility', 'visible');
            $('.modal-waiting').css('opacity', '1');
            

            if (this.IsAggrementChecked) {
                this.stripe.createToken(this.card).then(function (result) {
                    

                    if (result.error) {
                        // $scope.paymentError = true;
                        // $scope.paymentInfoSubmitted = false;
                        // $scope.$apply();
                        self.isCardInvalid = true;

                        $('.modal-waiting').css('visibility', 'hidden');
                        $('.modal-waiting').css('opacity', '0');
                    } else {
                        self.isCardInvalid = false;
                        console.log('token created, updating order payment info');
                        var data = {
                            orderId: self.orderId,
                            stripeToken: result.token.id,
                        }

                        axios.post(echic_config.$baseUrl + 'api/Order/UpdateOrder/', data).then(function(response){
                            self.$emit('nextStep', event);
                            $('.modal-waiting').css('visibility', 'hidden');
                            $('.modal-waiting').css('opacity', '0');
                        });
                    }
                });
            }
        },
        OnAgreementChanged(event){
            this.IsAggrementChecked = event.target.checked;
            this.IsFinishButtonDisabled = this.IsAggrementChecked;
        }
    },
    created: function(){
        var self = this;

        axios.get(echic_config.$baseUrl + 'api/Order/GetPulisableKey/')
            .then(function (response) {
                if(response.status === 200){
                    self.stripe = Stripe(response.data);
                    var elements = self.stripe.elements();

                    self.card = elements.create('cardNumber');
                    self.card.mount('#card-number');

                    self.card = elements.create('cardCvc');
                    self.card.mount('#card-cvv');

                    self.card = elements.create('cardExpiry');
                    self.card.mount('#card-expiry');
                }
            });
    },
    template:
    `
    <div class="second-step__checkout-step second-step__main-step_4" v-bind:class="[isOpen('payment')?'open':'', isDone('payment')?'done':'']">
        <form action="#" class="second-step__payment-form" v-on:submit.prevent="onSubmit">
            <h2 class="second-step__payment-form_title">Payment details</h2>
            <div class="second-step__card-info">
                <div class="second-step__card-type">
                    <img src="/static/images/checkout/credit-cards_amex.jpg" alt="">
                    <img src="/static/images/checkout/credit-cards_mastercard.jpg" alt="">
                    <img src="/static/images/checkout/credit-cards_visa.jpg" alt="">
                </div>
                <label class="second-step__card-number-wrap">
                    Card Number
                    <div class="second-step__card-number">
                        <div id="card-number"></div>
                    </div>
                    <div class="second-step__card-number_img" id="brand-icon"></div>
                </label>
                <div class="second-step__cvv-expriry-wrap">
                    <label class="second-step__card-expiry-wrap">
                        Expiry date
                        <div class="second-step__card-expiry">
                            <div id="card-expiry"></div>
                        </div>
                    </label>
                    <label class="second-step__card-cvv-wrap">
                        Cvv code
                        <div class="second-step__card-cvv">
                            <div id="card-cvv"></div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="second-step__payment-agreement">
                <span class="error-code" v-show="isCardInvalid">Your card could not be verified. Please check your payment details</span>
                <div class="second-step__payment_lock">
                    <img src="/static/images/checkout/padlock.png" alt="lock"> Your payment details are stored securely
                </div>
                <div class="second-step__payment_check">
                    <input type="checkbox" id="pay_check_1" v-on:change="OnAgreementChanged">
                    <label for="pay_check_1">
                        I agree with the
                        <a href="https://www.echic.world/docs/Booking Policy.pdf" target="_blank">Booking Policy</a>
                    </label>
                </div>
            </div>
            <div class="second-step__checkout-step-btn">
                <input type="button" class="second-step__checkout-step-back" value="Back" v-on:click="backStep">
                <input type="submit" class="second-step__payment-details-next second-step__checkout-step-next" value="Confirm order" v-bind:disabled="IsFinishButtonDisabled||isBusy">
            </div>
        </form>
        <div class="modal-waiting">
            <img src="/static/images/gif/waiting.gif" alt="">
                We are processing your order
        </div>
    </div>`
 });