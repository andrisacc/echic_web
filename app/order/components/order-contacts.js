Vue.component('order-contacts', {
    data: function () {
        return {
            phoneInfo: {}
        }
    },
    props: [
        'currentStep',
        'addresses',
        'phoneCode'
    ],
    methods:{
        isOpen: function(stepName){
            return stepName === this.currentStep.step;
        },
        isDone: function(stepName){
            return this.currentStep.steps.indexOf(stepName) < this.currentStep.steps.indexOf(this.currentStep.step);
        },
        phoneBackStep: function(){
            this.$emit('backStep', event);
        },
        phoneNextStep: function(event){
            this.phoneInfo = event;

            this.currentStep.step = "code";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        codeBackStep: function(){
            this.currentStep.step = "phonenumber";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        codeNextStep: function(){
            this.currentStep.step = "contacts";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        detailsBackStep: function(){
            this.currentStep.step = "phonenumber";
            $('body').scrollTop($(".second-step__content").offset().top);
        },
        detailsNextStep: function(event){
            this.$emit('nextStep', { phoneInfo: this.phoneInfo, contacts: event  } );
        }
    },
    template:
    `
    <div class="second-step__checkout-step second-step__main-step_3 "
        v-bind:class="[(isOpen('phonenumber')||isOpen('code')||isOpen('contacts'))?'open':'', (isDone('phonenumber')&&isDone('code')&&isDone('contacts'))?'done':'']">
        <div class="second-step__contact-tabs">
            <div class="second-step__contact-tabs-item second-step__verifi-phone_js" v-bind:class="[isOpen('phonenumber')?'active':'']">Account verification</div>
            <div class="second-step__contact-tabs-item second-step__verifi-code_js" v-bind:class="[isOpen('code')?'active':'']">Verification code</div>
            <div class="second-step__contact-tabs-item second-step__contact-details_js" v-bind:class="[isOpen('contacts')?'active':'']">Contact details</div>
        </div>

        <div class="second-step__verifi-account second-step__substep" v-bind:class="[isOpen('phonenumber')?'open':'', isDone('phonenumber')?'done':'']">
            <order-contacts-number
                v-bind:phoneCode="phoneCode"
                v-on:nextStep="phoneNextStep"
                v-on:backStep="phoneBackStep"></order-contacts-number>
        </div>

        <div class="second-step__verifi-code second-step__substep" v-bind:class="[isOpen('code')?'open':'', isDone('code')?'done':'']">
            <order-contacts-code
                v-bind:phoneInfo="phoneInfo"
                v-on:nextStep="codeNextStep"
                v-on:backStep="codeBackStep"></order-contacts-code>
        </div>

        <div class="second-step__contact-details second-step__substep" v-bind:class="[isOpen('contacts')?'open':'', isDone('contacts')?'done':'']">
            <order-contacts-details
                v-on:nextStep="detailsNextStep"
                v-on:backStep="detailsBackStep"
                v-bind:addresses="addresses"></order-contacts-details>
        </div>
    </div>`
 });