Vue.component('order-contacts-details', {
    data: function () {
        return {
            firstName: "",
            secondName: "",
            address: "",
            city: "",
            email: "",
            message: "",

            firstNameErrorStyle: "",
            lastNameErrorStyle: "",
            emailErrorStyle: "",
            addressErrorStyle: ""
        }
    },
    props: [
        'addresses'
    ],
    mounted: function(){
        
    },
    methods:{
        backStep: function(event){
            event.preventDefault();
            this.$emit('backStep', event);
        },
        nextStep: function(event){
            event.preventDefault();

            this.address = $( "#address" ).val();
            if(this.address != null){
                let parts = this.address.split(',');
                this.city = parts[parts.length - 1].replace(/\s+/g, '');
            }

            if(this.firstName.length == 0){
                this.firstNameErrorStyle = "1px solid rgb(232, 40, 40)";
            } else {
                this.firstNameErrorStyle = "";
            }

            if(this.secondName.length == 0){
                this.lastNameErrorStyle = "1px solid rgb(232, 40, 40)";
            } else {
                this.lastNameErrorStyle = "";
            }

            var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i;
            if(this.email.search(pattern) !== 0){
                this.emailErrorStyle = "1px solid rgb(232, 40, 40)";
            } else {
                this.emailErrorStyle = "";
            }

            if(this.secondName.length == 0){
                this.lastNameErrorStyle = "1px solid rgb(232, 40, 40)";
            } else {
                this.lastNameErrorStyle = "";
            }

            if($('.second-step__contact-box-address select').val() == ""){
                $('.select2-selection').css('border', '1px solid #e82828');
                flag = false;
            } else {
                $('.select2-selection').css('border', '');
            }

            if(this.firstName.length > 0 && this.secondName.length > 0 && this.address != null && this.address.length > 0 && this.email.length > 0 && this.email.search(pattern) === 0){
                var address = $('#address option:selected').text();
                var parts = address.split(',');
                var city = parts[parts.length - 1].replace(/\s+/g, '');
                var result = {
                    firstName: this.firstName,
                    secondName: this.secondName,
                    email: this.email,
                    address: address,
                    city: city,
                    message: this.message
                };
                this.$emit('nextStep', result);
            }
        }
    },
    template:
    `
    <form action="#" class="second-step__contact-form">
        <h2 class="second-step__contact-form_title">Enter your contact details</h2>
        <div class="second-step__contact-info">
        <div class="second-step__contact-box">
            <label class="second-step__contact-box_label">First name</label>
            <input type="text" class="second-step__contact-box_input" required v-model="firstName" v-bind:style="{ border: firstNameErrorStyle }">
        </div>
        <div class="second-step__contact-box">
            <label class="second-step__contact-box_label">Last name</label>
            <input type="text" class="second-step__contact-box_input" required v-model="secondName" v-bind:style="{ border: lastNameErrorStyle }">
        </div>
        <div class="second-step__contact-box-address">
            <label class="second-step__contact-box_label">Address</label>
            <select name="address" id="address" class="select2">
                <option></option>
                <option v-for="addr in addresses" value="addr.detailedAddress">{{addr.detailedAddress}}</option>
            </select>
        </div>
        <div class="second-step__contact-box-email">
            <label class="second-step__contact-box_label">Email</label>
            <input type="email" class="second-step__contact-box_input" required v-model="email" v-bind:style="{ border: emailErrorStyle }">
        </div>

        <div class="second-step__contact-box-message">
            <label class="second-step__contact-box_label">Message</label>
            <textarea type="text" class="second-step__contact-box_message" v-model="message"></textarea>
        </div>
        </div>
        <div class="second-step__checkout-step-btn">
            <input type="button" class="second-step__checkout-step-back" value="Back" v-on:click="backStep">
            <input type="button" class="second-step__checkout-main-steps second-step__checkout-step-next" value="Next" v-on:click="nextStep">
        </div>
    </form>`
 });