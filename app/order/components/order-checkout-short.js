Vue.component('order-checkout-short', {
    props: [
        'purchases'
    ],
    methods: {
        checkoutToggle: function(){
            var widthWin = $(window).width();

            var temp = $('.second-step__header-checkout-list').offset().left
            if($(window).width() <= 768){
                $('.second-step__header-checkout-body').css('width', '100%');
            }else{
                $('.second-step__header-checkout-body').css('width', widthWin - temp + 'px');
            }

            if($('.second-step__header-checkout-body').hasClass('open')){
                $('.second-step__header-checkout-body').toggleClass('open');
                $('.second-step__header-checkout-body').height('0px');
                $('.second-step__checkout-list_toggle').css('transform','rotate(-90deg)')
            }else{
                $('.second-step__header-checkout-body').toggleClass('open');
                var footerHeight = $('.footer').height() - 11;
                $('.second-step__header-checkout-body').css('height', 'calc(100% + '+ footerHeight +'px )');
                $('.second-step__checkout-list_toggle').css('transform','rotate(90deg)')
            }
        }
    },
    computed: {
        purchasesSum: function () {
            var sum = 0;
            if(this.purchases.length > 0){
                this.purchases.forEach(item => {
                    sum += item.quantity*item.cost/100;
                });
            }
            return sum;
        },
        serviceCounter: function () {
            var cnt = 0;
            if(this.purchases.length > 0){
                this.purchases.forEach(item => {
                    cnt += item.quantity;
                });
            }
            return cnt;
        }
    },
    template:
    `
    <div class="second-step__header-checkout-list" v-on:click="checkoutToggle">
        <img src="/static/images/checkout/Cart.png" alt="bill">
        <span class="second-step__checkout-list_total">£{{purchasesSum}}.00</span>
        <span class="second-step__checkout-list_total_mobile" v-show="serviceCounter>0">{{serviceCounter}}</span>
        <button class="second-step__checkout-list_toggle"></button>
    </div>
    `
 });