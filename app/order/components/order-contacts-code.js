Vue.component('order-contacts-code', {
    data: function () {
        return {
            code: "",
            isBusy: false,
            isWrongCode: false
        }
    },
    props: [
        'phoneInfo'
    ],
    methods:{
        backStep: function(event){
            event.preventDefault();
            this.$emit('backStep', event);
        },
        nextStep: function(event){
            event.preventDefault();

            var self = this;
            axios.get(echic_config.$baseUrl + "api/utilities/VerifyPhoneCode/?requestId=" + self.phoneInfo.requestId + "&code=" + self.code)
            .then(function (response) {
                
                if(response.status === 200 && response.data){
                    self.$emit('nextStep');
                    self.isWrongCode = false;

                    setTimeout(function(){
                        $('.second-step__ss_input').val('');
			            $('.second-step__ss-all').val('');
                    }, 1000)
                } else {
                    $('.second-step__ss_input').val('');
			        $('.second-step__ss-all').val('');
                    self.isWrongCode = true;
                }
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function(){
                self.isBusy = false;
            });
        }
    },
    template:
    `
    <form action="#" class="second-step__verifi-code-form">

        <h2 class="second-step__verifi-code-form_title">Verify account to continue</h2>
        <p class="second-step__verifi-code-form_subtitle">Enter verification code that was sent to your phone number below</p>

        <p class="second-step__phone-text">
        <span class="second-step__phone-text_code">{{phoneInfo.phoneCode}}</span>
        <span class="second-step__phone-text_numb">{{phoneInfo.phoneNumber}}</span>
        </p>

        <div class="second-step__verify_sms">
            <label class="second-step__ss-wrap">
                <input class="second-step__ss_input" type="text" readonly="readonly">
                <span class="second-step__outline"></span>
            </label>

            <label class="second-step__ss-wrap">
                <input class="second-step__ss_input" type="text" readonly="readonly">
                <span class="second-step__outline"></span>
            </label>

            <label class="second-step__ss-wrap">
                <input class="second-step__ss_input" type="text" readonly="readonly">
                <span class="second-step__outline"></span>
            </label>

            <label class="second-step__ss-wrap">
                <input class="second-step__ss_input" type="text" readonly="readonly">
                <span class="second-step__outline"></span>
            </label>
            <span class="error-code"  v-bind:style="{ display: isWrongCode?'block':'none' }">Incorrect Code. Please re-enter the code or click "Resend"</span>
            <p class="second-step__dont-code-wrap">
                I didn’t receive a code <br><a href="#" class="second-step__resend" v-on:click="backStep">Resend</a>
            </p>
            <input class="second-step__ss-all" type="tel" v-model="code">
            <div class="second-step__checkout-step-btn">
                <input type="button" class="second-step__checkout-step-back" value="Back" v-on:click="backStep">
                <input type="submit" class="second-step__checkout-main-steps second-step__checkout-step-next second-step__login-btn" value="Next" v-on:click="nextStep" value="Next" :disabled="code.length<4||isBusy">
            </div>
        </div>
    </form>`
 });