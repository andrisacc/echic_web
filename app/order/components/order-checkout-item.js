Vue.component('order-checkout-item', {
    props: [
        'purchase',
        'isEditable'
    ],
    methods: {
        minusClicked: function(event){
            event.preventDefault();
            if(this.purchase.quantity>1){
                this.purchase.quantity--;
            }
        },
        plusClicked: function(event){
            event.preventDefault();
            if(this.purchase.quantity<3){
                this.purchase.quantity++;
            }
        },
        purchaseRemoved: function(event){
            event.preventDefault();
            this.$emit('purchaseRemoved', this.purchase);
        }
    },
    template:
    `
        <div class="second-step__treatments-item">
            <div class="second-step__treatments-item-info">
                <div class="second-step__treatments_name">{{purchase.title}}</div>
                <div class="second-step__treatments_price">£ {{purchase.cost/100}}.00</div>
                <div class="second-step__treatments_duration">DURATION: {{purchase.duration}} MINUTES</div>
                <div v-if="!isEditable" class="second-step__treatments_item-count">{{purchase.quantity}}</div>
            </div>
            <div v-if="isEditable" class="second-step__treatments-item-control">
                <div class="second-step__treatments_remove" v-on:click="purchaseRemoved">Remove</div>
                <div class="second-step__treatments_counter">
                    <button class="second-step__minus" tabindex="0" v-on:click="minusClicked">-</button>
                    <input type="text" value="0" v-model="purchase.quantity" class="second-step__count_input" readonly="true" tabindex="0">
                    <button class="second-step__plus" tabindex="0" v-on:click="plusClicked">+</button>
                </div>
            </div>
        </div>
    `
 });