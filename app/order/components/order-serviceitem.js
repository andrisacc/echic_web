Vue.component('order-serviceitem', {
    props: [
        'serviceItem'
    ],
    methods: {
        minusClicked: function(event){
            event.preventDefault();
            if(this.serviceItem.quantity>0){
                this.serviceItem.quantity--;
            }
        },
        plusClicked: function(event){
            event.preventDefault();
            if(this.serviceItem.quantity < 3){
                this.serviceItem.quantity++;
            }
        },
        addClicked: function(event){
            event.preventDefault();
            this.$emit('serviceAdded', this.serviceItem);
        }
    },
    template:
    `
    <div class="second-step__packages-item">
        <div class="second-step__offer">
            <div class="second-step__name">{{serviceItem.title}}</div>
            <div class="second-step__descr">{{serviceItem.subTitle}}</div>
        </div>
        <div class="second-step__service-info">
            <div class="second-step__cost">
                <div class="second-step__name">Cost</div>
                <div class="second-step__descr">£{{serviceItem.cost/100}}</div>
            </div>
            <div class="second-step__quantity">
                <div class="second-step__name">Quantity</div>
                <div class="second-step__count">
                    <button class="second-step__minus" tabindex="0" v-on:click="minusClicked">-</button>
                    <input type="text" value="0" class="second-step__count_input" readonly="true" tabindex="0" v-model="serviceItem.quantity">
                    <button class="second-step__plus" tabindex="0" v-on:click="plusClicked">+</button>
                </div>
            </div>
            <div class="second-step__duration">
                <div class="second-step__name">Duration</div>
                <div class="second-step__descr">{{serviceItem.duration}} min</div>
            </div>
        </div>

        <div class="second-step__add">
            <button type="button" name="button" class="second-step__add_button" 
                :disabled="serviceItem.quantity<1" 
                v-bind:class="{'second-step__btn-disabled': serviceItem.quantity<1}" 
                v-on:click="addClicked" >Add</button>
        </div>

    </div>
    `
});