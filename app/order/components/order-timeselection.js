Vue.component('order-timeselection', {
    data: function () {
        return {
            allowedDays: [],
            selectedDate: null,
            selectedTime: null
        }
    },
    props: [
        "currentStep",
        "totalDuration"
    ],
    methods:{
        isOpen: function(stepName){
            return stepName === this.currentStep.step;
        },
        isDone: function(stepName){
            return this.currentStep.steps.indexOf(stepName) < this.currentStep.steps.indexOf(this.currentStep.step);
        },
        backStep: function(){
            this.$emit('backStep', event);
        },
        nextStep: function(){
            this.$emit('nextStep', event);
        },
        dateSelected: function(date){
            this.selectedDate = date;
            this.$emit('dateSelected', { date: this.selectedDate, time: this.selectedTime });
        },
        timeSelected: function(date){
            this.selectedTime = date;
            this.$emit('dateSelected', { date: this.selectedDate, time: this.selectedTime });
        }
    },
    template:
    `
    <div class="second-step__checkout-step second-step__main-step_2"
        v-bind:class="[isOpen('time')?'open':'', isDone('time')?'done':'']">
        <div class="second-step__checkout-body">
        <div class="second-step__bg-img">
            <img src="/static/images/checkout/hand_illustration.svg" alt="">
        </div>
        <form action="#" class="second-step__form-calendar">
            <h2 class="second-step__contact-form_title">Select date & time</h2>
            <order-date v-bind:totalDuration="totalDuration" v-on:dateSelected="dateSelected"></order-date>
            <order-time v-bind:selectedDate="selectedDate" v-on:timeSelected="timeSelected"></order-time>
            <div class="second-step__checkout-step-btn">
                <input type="button" class="second-step__checkout-step-back" value="Back" v-on:click="backStep">
                <input type="button" class="second-step__checkout-main-steps second-step__checkout-step-next" value="Next" v-on:click="nextStep" :disabled="selectedDate==null||selectedTime==null">
            </div>
        </form>
        </div>
    </div>`
 });