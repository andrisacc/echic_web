Vue.component('order-contacts-number', {
    data: function () {
        return {
            phoneNumber: "",
            isError: "",
            errorMessage: "",
            isBusy: false
        }
    },
    props: [
        'phoneCode'
    ],
    mounted: function(){
        $('.second-step__verifi-number_input').on('keydown', function(e){
            if(e.key.length == 1 && e.key.match(/[^0-9'".]/)){
            return false;
            };
        })
    },
    methods:{
        backStep: function(event){
            event.preventDefault();
            this.$emit('backStep', event);
        },
        nextStep: function(event){
            event.preventDefault();
            var self = this;
            setTimeout(function(){
                if (self.phoneNumber.startsWith("00")){
                    self.phoneNumber = self.phoneNumber.substring(2);
                } else if (self.phoneNumber.startsWith("0")){
                    self.phoneNumber = self.phoneNumber.substring(1);
                } else {
                    self.phoneNumber = self.phoneNumber;
                }
            }, 1000);

            this.isBusy = true;

            axios.post(echic_config.$baseUrl + 'api/utilities/SendPhoneVerificationCode', {
                PhoneCode: this.phoneCode,
                PhoneNumber: this.phoneNumber,
                IsCustomer: true
            }).then(function (response) {
                if(response.status === 200 && !response.data.isAlreadyRegistred){
                    self.$emit('nextStep', {
                        phoneCode: self.phoneCode,
                        phoneNumber: self.phoneNumber,
                        requestId: response.data.requestId
                    });
                } else {
                    self.errorMessage = "Sorry, already registred";
                    self.isError = true;
                }
            })
            .catch(function (error) {
                self.errorMessage = 'Sorry, connection error';
                self.isError = true;
            })
            .then(function(){
                self.isBusy = false;
            });
        },
        keyEntered: function(event){
            // this.isError = false;

            // this.phoneNumber = this.phoneNumber.replace(/\D/g,'');
            // if(this.phoneNumber.length >= 15){
            //     this.phoneNumber = this.phoneNumber.substr(0,15);
            // }
        },
    },
    template:
    `
    <form action="#" class="second-step__verifi-account-form">
        <h2 class="second-step__verifi-account-form_title">Verify account to continue</h2>
        <span class="second-step__verifi-account-form_subtitle">Enter your Phone number to get verification code</span>
        <div class="second-step__select">
            <div class="slct">({{phoneCode}})</div>
            <input type="tel" class="second-step__verifi-number_input" maxlength="15" placeholder="Phone number" v-model="phoneNumber" v-on:keyup="keyEntered">
            <span class="first-step__error-code" v-show="isError">{{errorMessage}}</span>
        </div>

        <div class="second-step__checkout-step-btn">
            <input type="button" class="second-step__checkout-step-back" value="Back" v-on:click="backStep">
            <input type="submit" class="second-step__checkout-main-steps second-step__checkout-step-next" value="Next" v-on:click="nextStep" value="Next" :disabled="isBusy||phoneNumber.length<6">
        </div>
    </form>`
 });