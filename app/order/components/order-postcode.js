Vue.component('order-postcode', {
    data: function () {
        return {
            isError: false,
            errorMessage: '',
            isBusy: false
        }
    },
    props: [
        'postcode'
    ],
    methods: {
        checkPostCode: function(event){
            event.preventDefault();
            if(this.postcode.length > 0 && !this.isBusy){
                $('.first-step__form_input-text').blur();
                this.isBusy = true;

                var self = this;
                axios.get(echic_config.$baseUrl + 'api/utilities/CheckPostalCode/' + this.postcode)
                .then(function (response) {
                    if(response.data.isAllowed && response.data.isExist && response.status === 200){
                        response.data.postcode = self.postcode;
                        self.$emit('nextStep', response.data);
                        $('body').scrollTop($(".second-step__content").offset().top);
                    } else {
                        self.errorMessage = "Sorry, it seems we don't work in your area yet";
                        self.isError = true;
                    }
                })
                .catch(function (error) {
                    self.errorMessage = 'Sorry, connection error';
                    self.isError = true;
                })
                .then(function(){
                    self.isBusy = false;
                });
            }
        },
        resetEvent: function(event){
            this.errorMessage = '';
            this.isError = false;
        }
    },
    template:
    `
    <form action="#" class="first-step__form">
        <span class="first-step__input-wrap not-valid">
            <input type="text" name="post" class="first-step__form_input-text" placeholder="Enter your postcode"
                v-model="postcode" 
                v-on:keyup="resetEvent" 
                v-on:keyup.enter="checkPostCode">
            <span class="error-code" v-bind:style="{ display: isError?'block':'none' }">{{errorMessage}}</span>
        </span>
        <input type="submit" value="Enter" class="first-step__form_input-submit" readonly="true"
            v-on:click="checkPostCode">
    </form>`
 });