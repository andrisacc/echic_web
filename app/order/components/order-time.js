Vue.component('order-time', {
    data: function(){
        return {
            selectedTime:{
                timeH: 0,
                timeM: 0
            },
            selectedDate: {},
            disableInc: false,
            disableDec: false,
            cnt: 0
        }
    },
    props: [
        'selectedDate'
    ],
    mounted: function(){
        var self = this;

        $('.second-step__change-time').mousedown(function (e) {
            e.preventDefault();
            if ($(this).hasClass('second-step__timepicker-next')) {
                self.timeInc();
            } else if ($(this).hasClass('second-step__timepicker-prev')) {
                self.timeDec();
            }
        });
        
        var changeTimeBtn = document.querySelectorAll('.second-step__change-time');
        for (var i = 0; i < changeTimeBtn.length; i++) {
            changeTimeBtn[i].addEventListener('touchstart', function (e) {
                 e.preventDefault();
                if (this.classList.contains('second-step__timepicker-next')) {
                    self.timeInc();
                } else if (this.classList.contains('second-step__timepicker-prev')) {
                    self.timeDec();
                }
            }, false);
        }
    },
    methods:{
        timeInc: function(){

            if(!this.disableInc){
                if(this.selectedTime.timeM == 0){
                    this.selectedTime.timeM = 30;
                } else {
                    this.selectedTime.timeM = 0;
                    this.selectedTime.timeH++;
                }
            }

            if(this.selectedTime.timeH == this.selectedDate.maxTimeH && this.selectedTime.timeM == this.selectedDate.maxTimeM){
                this.disableInc = true;
            } else {
                this.disableInc = false;
            }

            if(this.selectedTime.timeH == this.selectedDate.minTimeH && this.selectedTime.timeM == this.selectedDate.minTimeM){
                this.disableDec = true;
            } else {
                this.disableDec = false;
            }

            this.$emit('timeSelected', this.selectedTime);
        },
        timeDec: function(){

            if(!this.disableDec){
                if(this.selectedTime.timeM == 0){
                    this.selectedTime.timeM = 30;
                    this.selectedTime.timeH--;
                } else {
                    this.selectedTime.timeM = 0;
                }
            }

            if(this.selectedTime.timeH == this.selectedDate.maxTimeH && this.selectedTime.timeM == this.selectedDate.maxTimeM){
                this.disableInc = true;
            } else {
                this.disableInc = false;
            }

            if(this.selectedTime.timeH == this.selectedDate.minTimeH && this.selectedTime.timeM == this.selectedDate.minTimeM){
                this.disableDec = true;
            } else {
                this.disableDec = false;
            }

            this.$emit('timeSelected', this.selectedTime);
        },
        cntChanged: function(){
            console.log(this.cnt);
        }
    },
    watch:{
        selectedDate: function(newDate, oldDate){
            this.selectedTime.timeH = newDate.minTimeH;
            this.selectedTime.timeM = newDate.minTimeM;
            this.selectedDate = newDate;
            this.disableDec = true;
            this.disableInc = false;
            this.$emit('timeSelected', this.selectedTime);
        }
    },
    template:
    `
    <div id="timepicker" class="second-step__timepicker">
        <button type="button" class="second-step__change-time second-step__timepicker-next"></button>
        <div class="second-step__timepicker_hours">{{selectedTime.timeH.toString().length>1?selectedTime.timeH:'0'+selectedTime.timeH}}</div>
        <div class="second-step__timepicker_join">:</div>
        <div class="second-step__timepicker_minuts">{{selectedTime.timeM.toString().length>1?selectedTime.timeM:'0'+selectedTime.timeM}}</div>
        <button type="button" class="second-step__change-time second-step__timepicker-prev"></button>
    </div>`
 });