Vue.component('order-servicesgroup', {
    data: function () {
        return {
            isNextStepDisabled:true,
            nextBtnColor: '#757575'
        }
    },
    props:[
        'servicesGroup',
        'services',
        'purchases'
    ],
    methods:{
        serviceAdded: function(event){
            this.$emit('serviceAdded', event);
        },
        backStep: function(){
            this.$emit('backStep', event);
        },
        nextStep: function(){
            this.$emit('nextStep', event);
        }
    },
    watch: {
        purchases: function(newItem, oldItem){
            if(this.purchases.length > 0){
                this.isNextStepDisabled = false;
                this.nextBtnColor = '#5e0e8b'
            } else {
                this.isNextStepDisabled = true;
                this.nextBtnColor = '#757575'
            }
        }
    },
template:
    `
    <form action="#" class="second-step__form second-step__tabs-content" v-bind:class="{active: servicesGroup.isSelected}">
        <div class="second-step__packages">
            <order-serviceitem 
                v-for="service in services.filter(s => s.servicesGroupId == servicesGroup.id)" 
                v-bind:serviceItem="service"
                v-on:serviceAdded="serviceAdded($event)"></order-serviceitem>
        </div>
        <div class="second-step__checkout-step-btn">
            <input type="button" class="second-step__checkout-step-back" value="Back" v-on:click="backStep">
            <input type="button" class="second-step__checkout-main-steps second-step__checkout-step-next" v-on:click="nextStep" v-bind:disabled="isNextStepDisabled" v-bind:style="{ 'background-color': nextBtnColor }" value="Next">
        </div>
    </form>
    `
});