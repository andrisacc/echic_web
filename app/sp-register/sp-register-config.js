var app = new Vue({ 
    el: '#join',
    template: 
    `<div class="join" id="join">
        <sp-register-wizard></sp-register-wizard>
    </div>`,
    created() {
        document.addEventListener('beforeunload', this.handler)
    },
    methods: {
        handler: function handler(event) {
            gtag('event', 'user_action', { event_category : 'close_page' });
        }
    }
})

