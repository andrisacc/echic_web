Vue.component('sp-register-wizard', {
    data: function () {
        return {
            postcodeInfo: {},
            countries: [],
            phoneCode: '',
            phoneNumber: '',
            codeRequestId: '',
            spId: '',
            contacts: {}
        }
    },
    methods: {
        postcodeNextStep: function (event) {
            this.postcodeInfo = event;
            this.phoneCode = event.phoneCode;

            var self = this;
            axios.get(echic_config.$baseUrl + 'api/utilities/getcountries')
            .then(function (response) {
                if(response.data && response.status === 200){
                    self.countries = response.data;
                }
            })
            .catch(function (error) {
            })
            .then(function(){
                self.changeStepAndLog(1);
            });
        },
        phoneNumberNextStep: function(event){
            this.phoneCode = event.phoneCode;
            this.phoneNumber = event.phoneNumber;
            this.codeRequestId = event.requestId;

            this.changeStepAndLog(2);
        },
        phoneCodeNextStep: function(event){

            var self = this;
            axios.put(echic_config.$baseUrl + 'api/ServiceProvider/Create/')
            .then(function(response){
                if(response.status === 200){
                    self.spId = response.data;
                    axios.post(echic_config.$baseUrl + 'api/ServiceProvider/UpdatePhoneNumber/',{
                        Id: self.spId,
                        PhoneCode: self.phoneCode,
                        PhoneNumber: self.phoneNumber.substring(self.phoneCode.length + 2, self.phoneNumber.length)
                    })
                    .then(function(response){
                        if(response.status === 200){
                            axios.post(echic_config.$baseUrl + 'api/ServiceProvider/UpdatePostalCode/',{
                                Id: self.spId,
                                PostalCode: self.postcodeInfo.postcode
                            })
                            .then(function(response){
                                if(response.status === 200){
                                    self.changeStepAndLog(3);
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        },
        phoneCodePrevStep: function(event){
            this.changeStepAndLog(1);
        },
        contactsNextStep: function(event){
            this.contacts = event;
            this.changeStepAndLog(4);
        },
        docsNextStep: function(event){
            this.docs = event;
            this.changeStepAndLog(5);
        },
        stepChanged: function(event){
            this.changeStepAndLog(event);
        },
        changeStepAndLog: function(index){
            $('#carousel-workflowver').carousel(index);
            console.log('changeStepAndLog: ' + index);
            if(index === 0){
                gtag('config', echic_config.$gtagid, {'page_path': '/index.html#postcode'});
            }else if(index === 1){
                gtag('config', echic_config.$gtagid, {'page_path': '/index.html#phonenumber'});
            }else if(index === 3){
                gtag('config', echic_config.$gtagid, {'page_path': '/index.html#contacts'});
            }else if(index === 4){
                gtag('config', echic_config.$gtagid, {'page_path': '/index.html#docs'});
            }else if(index === 5){
                gtag('config', echic_config.$gtagid, {'page_path': '/index.html#done'});
            }
        }
    },
    template: 
    `<div class="container">
        <div class="title">Join now</div>
        <sp-steps v-on:stepChanged="stepChanged($event)"></sp-steps>
        <div id="carousel-workflowver" class="carousel slide" data-ride="carousel" data-interval="0">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <sp-postcode v-on:nextStep="postcodeNextStep($event)"></sp-postcode>
                </div>
                <div class="item">
                    <sp-phonenumber v-bind:phoneCode="phoneCode" v-on:nextStep="phoneNumberNextStep($event)"></sp-phonenumber>
                </div>
                <div class="item">
                    <sp-phonecode v-bind:requestId="codeRequestId" 
                        v-bind:phoneCode="phoneCode" 
                        v-bind:phoneNumber="phoneNumber" 
                        v-on:nextStep="phoneCodeNextStep($event)" 
                        v-on:prevStep="phoneCodePrevStep($event)"></sp-phonecode>
                </div>
                <div class="item">
                    <sp-contacts v-bind:countries="countries" 
                    v-bind:addresses="postcodeInfo.addresses" 
                    v-bind:spId="spId"
                    v-on:nextStep="contactsNextStep($event)"></sp-contacts>
                </div>
                <div class="item">
                    <sp-docs v-bind:spId="spId" v-on:wizardDone="docsNextStep($event)"></sp-docs>
                </div>
                <div class="item">
                    <sp-complite></sp-complite>
                </div>
            </div>
        </div>
    </div>`
  })