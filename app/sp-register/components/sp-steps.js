Vue.component('sp-steps', {
    data: function () {
        return {
            message: 'Hello!'
        }
    },
    methods: {
        stepClicked: function(step, stepIndex){
            if($($(step)[0].currentTarget).hasClass('current'))
            {
                //$('#carousel-workflowver').carousel(stepIndex);
                this.$emit('stepChanged', stepIndex);
            }
        }
    },
    template: 
    `<div class="steps">
        <ul class="clearfix">
            <li class="current" data-step="0" v-on:click="stepClicked($event, 0);">
                <div class="head">
                    Enter your postcode
                </div>
                <div class="line"></div>
                <div class="circle">
                    <div class="icon icon-postcode"></div>
                </div>
            </li>
            <li data-step="1" v-on:click="stepClicked($event, 1);">
                <div class="head">
                    Phone verification
                </div>
                <div class="line"></div>
                <div class="circle">
                    <div class="icon icon-phone"></div>
                </div>
            </li>
            <li data-step="3" v-on:click="stepClicked($event, 3);">
                <div class="head">
                    Contact details
                </div>
                <div class="line"></div>
                <div class="circle">
                    <div class="icon icon-contact"></div>
                </div>
            </li>
            <li data-step="4" v-on:click="stepClicked($event, 4);">
                <div class="head">
                    Upload the documents
                </div>
                <div class="line"></div>
                <div class="circle">
                    <div class="icon icon-documents"></div>
                </div>
            </li>
        </ul>
    </div>`
})