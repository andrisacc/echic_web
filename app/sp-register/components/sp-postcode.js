Vue.component('sp-postcode', {
    data: function () {
        return {
            postcode: '',
            isError: false,
            errorMessage: '',
            isBusy: false
        }
    },
    methods: {
        checkPostCode: function (event) {
            this.isBusy = true;

            var self = this;
            axios.get(echic_config.$baseUrl + 'api/utilities/CheckPostalCode/' + this.postcode)
            .then(function (response) {
                if(response.data.isAllowed && response.data.isExist && response.status === 200){
                    response.data.postcode = self.postcode;
                    self.$emit('nextStep', response.data);
                } else {
                    self.errorMessage = "Sorry, it seems we don't work in your area yet";
                    gtag('event', 'user_error', { event_category : 'postal-code', 'event_label': 'notAllowed' });
                    self.isError = true;
                }
            })
            .catch(function (error) {
                self.errorMessage = 'Sorry, connection error';
                self.isError = true;
            })
            .then(function(){
                self.isBusy = false;
            });
        },
        resetEvent: function(event){
            this.errorMessage = '';
            this.isError = false;
        }
    },
    template: 
    `<div class="step-postcode">
        <div class="enter-postcode">Enter your post code to get started</div>
        <div class="form-group">
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1">
                <picture>
                    <source srcset="assets/images/Location.png, assets/images/Location@2x.png 2x" />
                    <img src="assets/images/Location.png" alt="Requirements" />
                </picture>
                </span>
                <input type="text" class="form-control" placeholder="Enter your postcode" 
                    v-model="postcode" 
                    v-on:keyup="resetEvent" 
                    v-on:keyup.enter="checkPostCode"/>
            </div>
        </div>
        <div class="form-group text-center">
            <p class="text-danger" v-show="isError"> {{errorMessage}}</p>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-primary" value="Enter" 
            v-bind:disabled="isBusy||postcode.length==0"
            v-on:click="checkPostCode" />
        </div>
    </div>`
})