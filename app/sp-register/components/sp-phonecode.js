Vue.component('sp-phonecode', {
    data: function () {
        return {
            code: '',
            isBusy: false,
            isWrongCode: false
        }
    },
    props: ['requestId', 'phoneCode', 'phoneNumber'],
    methods: {
        codeDigitEntered: function(){
            this.code = "";
            this.code += $($('.verification-code-value')[0]).val();
            this.code += $($('.verification-code-value')[1]).val();
            this.code += $($('.verification-code-value')[2]).val();
            this.code += $($('.verification-code-value')[3]).val();
            this.isWrongCode = false;
        },
        verifyPhoneCode: function(){
            if(this.code.length === 4){
                this.isBusy = true;

                var self = this;
                axios.get(echic_config.$baseUrl + "api/utilities/VerifyPhoneCode/?requestId=" + self.requestId + "&code=" + self.code)
                .then(function (response) {
                    if(response.status === 200 && response.data){
                        self.$emit('nextStep');
                        self.isWrongCode = false;
                    } else {
                        self.isWrongCode = true;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function(){
                    self.isBusy = false;
                    self.code = '';
                    $($('.verification-code-value')[0]).val("");
                    $($('.verification-code-value')[1]).val("");
                    $($('.verification-code-value')[2]).val("");
                    $($('.verification-code-value')[3]).val("");
                });
            }
        },
        prevClick: function(){
            this.$emit('prevStep');
        }
    },
    template: 
    `<div class="step-phone-verify">
        <div class="step-title">Verify account to continue</div>
        <div class="step-desc">Enter verification code we sent to phone number below<br/>{{phoneNumber}}</div>
        <div class="form-group checkcode">
            <input type="text" maxlength="1" class="form-control input-number input-lg text-center verification-code-value" v-on:keyup="codeDigitEntered" />
            <input type="text" maxlength="1" class="form-control input-number input-lg text-center verification-code-value" v-on:keyup="codeDigitEntered" />
            <input type="text" maxlength="1" class="form-control input-number input-lg text-center verification-code-value" v-on:keyup="codeDigitEntered" />
            <input type="text" maxlength="1" class="form-control input-number input-lg text-center verification-code-value" v-on:keyup="codeDigitEntered"
                v-on:keyup.enter="verifyPhoneCode" />
        </div>
        <div class="form-group text-center" v-show="isWrongCode">
            <p class="text-danger">Wrong code</p>
        </div>
        <div class="checkcode-help">I didn’t receive a code</div>
        <div class="checkcode-resend"><a href="#" v-on:click="prevClick">Resend</a></div>
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-primary" value="Enter"
                v-bind:disabled="isBusy||code.length<4"
                v-on:click="verifyPhoneCode" />
        </div>
    </div>`
})