Vue.component('sp-contacts', {
    data: function () {
        return {
            FirstName: '',
            LastName: '',
            Email: '',
            Address: '',
            City: '',
            Nationality: 'United Kingdom',

            firstNameHasError: false,
            lastNameHasError: false,
            emailHasError: false,
            addressHasError: false,
            IsEmailAlreadyRegistred: false,

            isBusy: false
        }
    },
    props: ['countries', 'addresses', 'spId'],
    methods: {
        countyChanged: function(event){
            this.Nationality = event.target.value;
        },
        addressChanged: function(event){
            this.Address = event.target.value;
            let parts = this.Address.split(',');
            this.City = parts[parts.length - 1].replace(/\s+/g, '');
            this.addressHasError = false;
        },
        inputChanged: function(source){
            if(source === "firstname"){
              this.firstNameHasError = false;
            }
        
            if(source === "lastname"){
              this.lastNameHasError = false;
            }
        
            if(source === "email"){
                this.emailHasError = false;
                this.IsEmailAlreadyRegistred = false;
            }
        },
        formSubmited: function(event){
            if(typeof this.FirstName=='undefined' || !this.FirstName){
                this.firstNameHasError = true;
                gtag('event', 'user_error', { event_category : 'contacts', 'event_label': 'firstNameEmpty' });
            }
        
            if(typeof this.LastName=='undefined' || !this.LastName){
                this.lastNameHasError = true;
                gtag('event', 'user_error', { event_category : 'contacts', 'event_label': 'lastNameEmpty' });
            }
        
            if(typeof this.Email=='undefined' || !this.Email || !this.Email.includes("@") || !this.Email.includes(".")){
                this.emailHasError = true;
                gtag('event', 'user_error', { event_category : 'contacts', 'event_label': 'wrongEmail' });
            }
            
            if(typeof this.Address=='undefined' || !this.Address){
                this.addressHasError = true;
                gtag('event', 'user_error', { event_category : 'contacts', 'event_label': 'wrongAddress' });
            }
        
            if(!this.firstNameHasError && !this.lastNameHasError && !this.addressHasError && !this.emailHasError){
                this.isBusy = true;
                var self = this;

                var cnts = {
                    Id: this.spId,
                    FirstName: this.FirstName,
                    LastName: this.LastName,
                    Email: this.Email,
                    Address: this.Address,
                    City: this.City,
                    Nationality: this.Nationality
                };

                axios.post(echic_config.$baseUrl + 'api/ServiceProvider/UpdateContacts/', cnts)
                .then(function (response) {
                    if(response.status === 200 && response.data.isSuccess){
                        self.$emit('nextStep', cnts);
                    } else if(response.status === 200 && response.data.isEmailAlreadyRegistred){
                        self.IsEmailAlreadyRegistred = true;
                        self.emailHasError = true;
                    } 
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function(){
                    self.isBusy = false;
                });
            }
        }
    },
    template: 
    `<div class="step-contact">
        <div class="step-title">Enter your contact details</div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group" v-bind:class="{ 'has-error': firstNameHasError }">
                    <label>First name</label>
                    <input type="text" class="form-control input-lg" v-model="FirstName" v-on:keyup="inputChanged('firstname')"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group" v-bind:class="{ 'has-error': lastNameHasError }">
                    <label>last name</label>
                    <input type="text" class="form-control input-lg"  v-model="LastName" v-on:keyup="inputChanged('lastname')"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group" v-bind:class="{ 'has-error': addressHasError }">
                    <label>address</label>
                    <select name="" id="" class="form-control input-lg" v-on:change="addressChanged">
                        <option disabled selected>Choose Address</option>
                        <option v-for="addr in addresses">{{addr.detailedAddress}}</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group" v-bind:class="{ 'has-error': emailHasError }">
                    <label>email</label>
                    <input type="email" class="form-control input-lg"  v-model="Email" v-on:keyup="inputChanged('email')"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label>Nationality</label>
                    <select name="" id="" class="form-control input-lg" v-on:change="countyChanged">
                        <option v-for="country in countries" v-bind:selected="country=='United Kingdom'">{{country}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group text-center" v-show="IsEmailAlreadyRegistred">
                <p class="text-danger">Email already registred</p>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-primary" value="Next" 
            v-bind:disabled="isBusy"
            v-on:click="formSubmited" />
        </div>
    </div>`
})