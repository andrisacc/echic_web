Vue.component('sp-uploader', {
    data: function () {
        return {
            state: "ready",//ready, wrongFile, fileSelected, uploading, uploaded, uploadFailed, uploadAborted 
            uploadProgress: 0,
            selectedFile: {},
            currentRequest: {},
            uploadedFileName: ''
        }
    },
    props: ['spId', 'uploaderTitle', 'uploaderSubtitle', 'docType'],
    methods: {
        chooseFileClicked: function(event){
            $(event.target).closest(".input-group").find('input[type="file"]').trigger('click');
        },
        fileChosen: function(event){
            console.log(event);
            if(event.currentTarget.files && event.currentTarget.files.length > 0){
                this.selectedFile = event.currentTarget.files[0];
                if(this.selectedFile.size > 10*1024*1024){
                    this.state = "wrongFile";
                    gtag('event', 'user_error', { event_category : 'docs', 'event_label': this.docType + '-sizeError' });
                } else {
                    this.state = "fileSelected";
                }
            }
        },        
        uploadClicked: function(){
            this.state = "uploading";
        
            var self = this;

            const config = {
                onUploadProgress: function(progressEvent) {
                    self.uploadProgress = (event.loaded / event.total) * 100;
                }
            }

            const formData = new FormData();
            formData.append('file', this.selectedFile, this.selectedFile.name);
            formData.append('serviceProviderId', this.spId);
            formData.append('docType', this.docType);

            axios.post(echic_config.$baseUrl + 'api/files/', formData, config)
            .then(function(response){
                self.$emit('fileUploaded', {
                    filename: response.data.filename
                });
                self.state = "uploaded";
                self.uploadedFileName = response.data.filename;
                self.currentRequest = null;
            })
            .catch(function (error) {
                self.state = "uploadFailed";
                self.currentRequest = null;
                self.uploadProgress = 0;
                self.uploadedFileName = null;
                console.log(error);
            });
        },
        deleteClicked: function(event){
            var self = this;
            this.state = "uploadAborted";
            $(event.target).closest(".form-group").find('input[type="file"]').val('');
            if(this.uploadedFileName != null){
                axios.delete(echic_config.$baseUrl + 'api/files/?filename=' + this.uploadedFileName + '&serviceProviderId=' + this.spId)
                .then(function(response){})
                .catch(function (error) {
                    
                    console.log(error);
                })
                .then(function(){
                    self.uploadedFileName = null;
                    self.$emit('fileUploaded', {
                        filename: ''
                    });
                    self.uploadProgress = 0;
                    self.state = "ready"
                });
            } else {
                this.uploadedFileName = null;
                self.$emit('fileUploaded', {
                    filename: ''
                });
                this.uploadProgress = 0;
                this.state = "ready"
            }
        }
    },
    template: 
    `<div><label>{{uploaderTitle}}<span class="ttn">{{uploaderSubtitle}}</span></label>
    <input type="hidden" name="passport-file-url" required />
    <div class="form-border form-control input-lg" v-bind:class="{ 'hidden': state!='ready'&&state!='wrongFile'&&state!='fileSelected' }">
        <div class="input-group">
            <span class="btn-file form-control input-lg">
                <span class="file-name" v-if="state === 'ready'"></span>
                <span class="file-name" v-if="state === 'wrongFile'">{{selectedFile.name}} is too large</span>
                <span class="file-name" v-if="state === 'fileSelected'">{{selectedFile.name}}</span>
                <span class="file-name" v-if="state === 'uploading'"></span>
                <span class="file-name" v-if="state === 'uploaded'"></span>
                <span class="file-name" v-if="state === 'uploadFailed'"></span>
                <span class="file-name" v-if="state === 'uploadAborted'"></span>
                <input type="file" name="passport-file" v-on:change="fileChosen"/>
            </span>
            <span class="input-group-btn" v-bind:class="{ 'hidden': state!='ready'&&state!='wrongFile'}">
                <button class="btn btn-primary btn-lg btn-choose" type="button" v-on:click.prevent="chooseFileClicked">Choose file</button>
            </span>
            <span class="input-group-btn" v-bind:class="{ 'hidden': state!='fileSelected'}">
                <button class="btn btn-primary btn-lg btn-send" type="button" v-on:click="uploadClicked">Upload file</button>
            </span>
        </div>
    </div>
    <div class="uploader-progress-bar" v-bind:class="{ 'hidden': state!='uploading'&&state!='uploaded'&&state!='uploadFailed'&&state!='uploadAborted'}">
        <div class="progress-bar-bg">
            <div class="progress-bar-line" v-bind:style="{width:uploadProgress+'%'}"></div>
            <div class="file-name" v-if="state === 'uploading'||state === 'uploaded'||state === 'uploadFailed'||state === 'uploadAborted'">{{selectedFile.name}}</div>
            <div class="file-status" v-if="state === 'uploading'" >Uploading…</div>
            <div class="file-status" v-if="state === 'uploaded'" >Upload is completed</div>
            <div class="file-status" v-if="state === 'uploadFailed'" >Upload Failed…</div>
            <div class="file-status" v-if="state === 'uploadAborted'" >Upload Aborted… Please wait…</div>
            <div class="file-delete"><a href="#" v-on:click="deleteClicked"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
        </div>
    </div></div>`
})