Vue.component('sp-complite', {
    data: function () {
        return {
            message: 'Hello!'
        }
    }
    ,
    template: 
    `<div class="step-complete">
        <img src="assets/images/complete_icon.jpg" alt="#"><Br/>
        ALL DONE!<br/>
        We will be in touch with regards<br/>
        to the next (interview) stage in no time!
    </div>`
})