Vue.component('sp-docs', {
    data: function () {
        return {
            PassportFilename: '',
            NiNumberFilename: '',
            AddressProofFilename: '',
            CvFilename: '',
            WorkPermitFilename: '',

            PassportFilenameHasError: false,
            NiNumberFilenameHasError: false,
            AddressProofFilenameHasError: false,
            CvFilenameHasError: false,

            IsAggrementChecked: false,
            IsFinishButtonDisabled: true,
            isBusy: false
        }
    },
    props: ['spId'],
    methods: {
        passportUploaded: function(passportFilename){
            this.PassportFilename = passportFilename; 
            console.log("uploaded to register comp passport filename: " + passportFilename);
            this.PassportFilenameHasError = false;
            this.IsFinishButtonDisabled = this.IsAggrementChecked && this.areFilesOk()
        },
        niNumberUploaded: function(niNumberFilename){
            this.NiNumberFilename = niNumberFilename;
            console.log("uploaded to register comp ni filename: " + niNumberFilename);
            this.NiNumberFilenameHasError = false;
            this.IsFinishButtonDisabled = this.IsAggrementChecked && this.areFilesOk()
        },
        addressProofUploaded: function(addressProofFilename){
            this.AddressProofFilename = addressProofFilename;
            console.log("uploaded to register comp address filename: " + addressProofFilename);
            this.AddressProofFilenameHasError = false;
            this.IsFinishButtonDisabled = this.IsAggrementChecked && this.areFilesOk()
        },
        cvUploaded: function(cvFilename){
            this.CvFilename = cvFilename;
            console.log("uploaded to register comp cv filename: " + cvFilename);
            this.CvFilenameHasError = false;
            this.IsFinishButtonDisabled = this.IsAggrementChecked && this.areFilesOk()
        },
        workPermitUploaded: function(workPermitFilename){
            this.WorkPermitFilename = workPermitFilename;
            console.log("uploaded to register comp workPermit filename: " + workPermitFilename);
        },
        areFilesOk: function(){
            if(!this.PassportFilename){
                this.PassportFilenameHasError = true;
                gtag('event', 'user_error', { event_category : 'docs', 'event_label': 'passportEmpty' });
            }
        
            if(!this.NiNumberFilename){
                this.NiNumberFilenameHasError = true;
                gtag('event', 'user_error', { event_category : 'docs', 'event_label': 'niEmpty' });
            }
        
            if(!this.AddressProofFilename){
                this.AddressProofFilenameHasError = true;
                gtag('event', 'user_error', { event_category : 'docs', 'event_label': 'addressProofEmpty' });
            }
        
            if(!this.CvFilename){
                this.CvFilenameHasError = true;
                gtag('event', 'user_error', { event_category : 'docs', 'event_label': 'cvEmpty' });
            }
        
            return !(this.PassportFilenameHasError || this.NiNumberFilenameHasError || this.AddressProofFilenameHasError || this.CvFilenameHasError);
        },
        nextStepClicked: function(){
            var self = this;
            this.isBusy = true;
            if(this.areFilesOk()){
                axios.post(echic_config.$baseUrl + 'api/ServiceProvider/UpdateAgree/',{
                    Id: self.spId,
                    IsAgree: true
                })
                .then(function(response){
                    if(response.status === 200){
                        self.$emit('wizardDone',{
                            PassportFilename: self.PassportFilename,
                            NiNumberFilename: self.PassportFilename,
                            AddressProofFilename: self.PassportFilename,
                            CvFilename: self.PassportFilename,
                            WorkPermitFilename: self.WorkPermitFilename
                        });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function(){
                    self.isBusy = false;
                });
            }
        },
        OnAgreementChanged(event){
            this.IsAggrementChecked = event.target.checked 
            this.IsFinishButtonDisabled = this.IsAggrementChecked && this.areFilesOk()
        }
    },
    template: 
    `<div class="step-documents">
        <div class="step-title">Upload your documents</div>
        <div class="step-desc">Max doc size 10MB</div>

        <div class="form-group file-uploader" v-bind:class="{ 'has-error': PassportFilenameHasError }">
            <sp-uploader doc-type="passport" v-bind:spId="spId" uploader-title="* PASSPORT" uploader-subtitle="" 
                v-on:fileUploaded="passportUploaded($event)"></sp-uploader>
        </div>

        <div class="form-group file-uploader" v-bind:class="{ 'has-error': NiNumberFilenameHasError }">
            <sp-uploader doc-type="ninumber" v-bind:spId="spId" uploader-title="* NI NUMBER " uploader-subtitle="(photo of the card or a letter about NI number)"
                v-on:fileUploaded="niNumberUploaded($event)"></sp-uploader>
        </div>

        <div class="form-group file-uploader" v-bind:class="{ 'has-error': AddressProofFilenameHasError }">
            <sp-uploader doc-type="addressproof" v-bind:spId="spId" uploader-title="* PROOF OF ADDRESS " uploader-subtitle="(photo of a tenancy agreement, an utility bill or a bank statement)"
                v-on:fileUploaded="addressProofUploaded($event)"></sp-uploader>
        </div>

        <div class="form-group file-uploader" v-bind:class="{ 'has-error': CvFilenameHasError }">
            <sp-uploader doc-type="cv" v-bind:spId="spId" uploader-title="* CV" uploader-subtitle=""
                v-on:fileUploaded="cvUploaded($event)"></sp-uploader>
        </div>

        <div class="form-group file-uploader">
            <sp-uploader doc-type="workpermit" v-bind:spId="spId" uploader-title="PROOF OF THE RIGHT TO WORK " uploader-subtitle="(Visa/Residence Card) (applicable for non-EU citizen)"
                v-on:fileUploaded="workPermitUploaded($event)"></sp-uploader>
        </div>
        
        <div class="form-group text-center agreement">
            <input type="checkbox" id="agreement" v-on:change="OnAgreementChanged"><label for="agreement">I have read and accept the <a href="#">eChic Beauticians' Service Agreement</a></label>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-primary" value="Finish registration" v-bind:disabled="!IsFinishButtonDisabled||isBusy" v-on:click="nextStepClicked"/>
        </div>
    </div>`
})