Vue.component('sp-phonenumber', {
    data: function () {
        return {
            phoneNumber: '',
            isBusy: false,
            isError: false,
            errorMessage: '',
            phoneCodePlaceholder: ''
        }
    },
    props: ['phoneCode'],
    methods: {
        verifyPhoneNumber: function(){
            if(this.phoneNumber.length >= 10){
                this.isBusy = true;

                var phoneNumberWithoutCode = this.phoneNumber.substring(this.phoneCode.length + 2, this.phoneNumber.length);

                var self = this;
                axios.post(echic_config.$baseUrl + 'api/utilities/SendPhoneVerificationCode', {
                    PhoneCode: this.phoneCode,
                    PhoneNumber: phoneNumberWithoutCode
                })
                .then(function (response) {
                    if(response.status === 200 && !response.data.isAlreadyRegistred){
                        self.$emit('nextStep', {
                            phoneCode: self.phoneCode,
                            phoneNumber: self.phoneNumber,
                            requestId: response.data.requestId
                        });
                    } else {
                        self.errorMessage = "Sorry, already registred";
                        self.isError = true;
                    }
                })
                .catch(function (error) {
                    self.errorMessage = 'Sorry, connection error';
                    self.isError = true;
                })
                .then(function(){
                    self.isBusy = false;
                });
            }
        },
        keyEntered: function(event){
            this.isError = false;

            if(this.phoneNumber.length == 1){
                this.phoneNumber = "(" + this.phoneCode + ")" + this.phoneNumber; 
            }

            var tmp = this.phoneNumber.substring(this.phoneCode.length + 2, this.phoneNumber.length);
            tmp = tmp.replace(/\D/g,'');
            this.phoneNumber = "(" + this.phoneCode + ")" + tmp;
        },
        onBlur: function(){
            if(this.phoneNumber.length <= this.phoneCode.length + 2){
                this.phoneNumber = "";
            }
        }
    },
    watch:{
        phoneCode: function(newPhoneCode, oldPhoneCode){
            this.phoneCodePlaceholder = '(' + newPhoneCode + ') Phone number';
        }
    },
    template: 
    `<div class="step-phone">
        <div class="step-title">Verify account to continue</div>
        <div class="step-desc">Enter your Phone number to get verification code</div>
        <div class="form-group bm">
            <input type="text" class="form-control input-lg line phone-mask text-center" v-bind:placeholder="phoneCodePlaceholder" maxlength="21" 
                v-on:blur="onBlur"
                v-on:keyup="keyEntered"
                v-model="phoneNumber"
                v-on:keyup.enter="verifyPhoneNumber"/>
        </div>
        <div class="form-group text-center" v-show="isError">
            <p class="text-danger">{{errorMessage}}</p>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-primary" value="Enter" 
                v-bind:disabled="isBusy||phoneNumber.length<10"
                v-on:click="verifyPhoneNumber"/>
        </div>
    </div>`
})